use [db28pr10]
go
--DROP TABLE HR.DEPS;
--DROP TABLE HR.USRS;
DROP TABLE [dbo].[DEPS];
DROP TABLE [dbo].[USRS];

CREATE TABLE [dbo].[DEPS]
(
	id INT  NOT NULL,
	name VARCHAR(128) NOT NULL
);
CREATE TABLE [dbo].[USRS]
(
	id INT  NOT NULL,
	id_deps INT NOT NULL,
	name VARCHAR(128)
);
--ALTER TABLE [dbo].[DEPS] ADD CONSTRAINT PK_DEPS PRIMARY KEY (ID);
--ALTER TABLE [dbo].[USRS] ADD CONSTRAINT PK_USRS PRIMARY KEY (ID);
--ALTER TABLE [dbo].[USRS] ADD CONSTRAINT FK_USRS FOREIGN KEY (id_deps) REFERENCES HR.DEPS(ID);

insert into [dbo].[DEPS] (id,name) values (1,'Sells');
insert into [dbo].[DEPS](id,name) values (2,'Support');
insert into [dbo].[DEPS] (id,name) values (3,'Fin');
insert into [dbo].[DEPS] (id,name) values (4,'Logistic');

insert into [dbo].[USRS] (id,name,id_deps) values(1,'��������',1);
insert into [dbo].[USRS] (id,name,id_deps) values(2,'�����',2);
insert into [dbo].[USRS] (id,name,id_deps) values(3,'���������',6);
insert into [dbo].[USRS] (id,name,id_deps) values(4,'�����',2);
insert into [dbo].[USRS] (id,name,id_deps) values(5,'����',4);