use master
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr10') DROP DATABASE db28pr10
go
CREATE DATABASE db28pr10
go
use [db28pr10]
go
CREATE TABLE [dbo].[CUSTOMERS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NICKNAME] [nvarchar](50) NULL,
	[PHONE] [nchar](20) NOT NULL,
	[EMAIL] [nchar](50) NOT NULL,
	[ID_MEN] [int] NULL
 )
go
CREATE TABLE [dbo].[POSITIONS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ROLE] [nchar](20) NULL
 )
go
CREATE TABLE [dbo].[MEN](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[LNAME] [nvarchar](50) NOT NULL,
	[YEAR] [int] NOT NULL
 )
go
CREATE TABLE [dbo].[EMPLOYEES](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[ID_MEN] [int] NOT NULL,
	[CODE] [nvarchar](50) NULL,
	[ID_POSITIONS] [int] NOT NULL
 )
 go
CREATE TABLE [dbo].[MANAGERS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_EMPLOYEES] [int] NULL,
	[ID_POSITIONS] [int] NULL
 )
 go
CREATE TABLE [dbo].[DEPARTMENTS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_MANAGER] [int] NULL
 )

 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES (N'�����������');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('It-dev');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('It-QA');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('Accounters');

 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'������', N'����', 1977);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'�������', N'����', 1980);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'������', N'����', 1975);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'�������', N'����', 1970);

 INSERT INTO [dbo].[POSITIONS] (NAME, ROLE) VALUES (N'��������', 'D');
 INSERT INTO [dbo].[POSITIONS] (NAME, ROLE) VALUES (N'Manager', 'M');
 INSERT INTO [dbo].[POSITIONS] (NAME) VALUES (N'�������');
 INSERT INTO [dbo].[POSITIONS] (NAME) VALUES (N'Developer');
 
 UPDATE [dbo].[POSITIONS]
 SET [dbo].[POSITIONS].ROLE = N'NoRole'
 WHERE [dbo].[POSITIONS].ROLE is NULL;

 INSERT INTO [dbo].[CUSTOMERS] (PHONE, EMAIL, ID_MEN) VALUES ('+380665841245', 'e-mail@gmail.com', 1);
 INSERT INTO [dbo].[CUSTOMERS] (EMAIL, PHONE, ID_MEN) VALUES ('e-mail2@yandex.ru', '+380684125285', 2);
 INSERT INTO [dbo].[CUSTOMERS] (EMAIL, PHONE, NICKNAME, ID_MEN) VALUES ('e-mail3@yandex.ru', '+380502514785', 'clientnick', 3);
 INSERT INTO [dbo].[CUSTOMERS] (PHONE, EMAIL) VALUES ('+380654125478', 'e-mail22@gmail.com');

 UPDATE [dbo].[CUSTOMERS]
 SET [dbo].[CUSTOMERS].NICKNAME = N'NoName'
 WHERE [dbo].[CUSTOMERS].NICKNAME is NULL;

 INSERT INTO [dbo].[EMPLOYEES] (ID_MEN, ID_POSITIONS) VALUES (1, 2);
 INSERT INTO [dbo].[EMPLOYEES] (ID_MEN, ID_POSITIONS) VALUES (2, 2);
 INSERT INTO [dbo].[EMPLOYEES] (ID_MEN, ID_POSITIONS) VALUES (3, 1);
 INSERT INTO [dbo].[EMPLOYEES] (ID_MEN, ID_POSITIONS) VALUES (4, 3);

 INSERT INTO [dbo].[MANAGERS] (NAME, ID_EMPLOYEES, ID_POSITIONS) VALUES (N'������� ���������', NULL, NULL );
 INSERT INTO [dbo].[MANAGERS] (NAME) VALUES (N'�������������� ��������');
 INSERT INTO [dbo].[MANAGERS] (NAME) VALUES (N'���������� ��������');


 use [db28pr10]
go
SELECT
	*
FROM
[dbo].[DEPARTMENTS]

go
SELECT
	*
FROM
[dbo].[MEN]
go
SELECT
	*
FROM
[dbo].[POSITIONS]
go
SELECT
[dbo].[MEN].NAME as MEN_NAME, --���������
	[dbo].[MEN].LNAME,
	[dbo].[MEN].YEAR,
	[dbo].[CUSTOMERS].NICKNAME,
	[dbo].[CUSTOMERS].PHONE,
	[dbo].[CUSTOMERS].EMAIL,
	[dbo].[CUSTOMERS].ID_MEN
FROM
[dbo].[CUSTOMERS]
LEFT JOIN [dbo].[MEN] --������������� �������, c LEFT JOIN ����� ������������ ������ �� ������ MEN, ������� ������� � CUSTOMERS
ON [dbo].[CUSTOMERS].ID_MEN = [dbo].[MEN].ID
go
SELECT
	[dbo].[EMPLOYEES].ID as EMPLOYEE_ID,
	[dbo].[MEN].NAME as MEN_NAME, --���������
	[dbo].[MEN].LNAME,
	[dbo].[MEN].YEAR,
	[dbo].[POSITIONS].NAME as POSITION_NAME,
	[dbo].[POSITIONS].ROLE
FROM
[dbo].[EMPLOYEES]
FULL JOIN [dbo].[MEN] --������������� �������, ����� ������� ������ ������� ������
ON [dbo].[EMPLOYEES].ID_MEN = [dbo].[MEN].ID
FULL JOIN [dbo].[POSITIONS] 
ON [dbo].[EMPLOYEES].ID_POSITIONS = [dbo].[POSITIONS].ID

go
SELECT
	[dbo].[MANAGERS].NAME
FROM
[dbo].[MANAGERS]