use[MyExamDB]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'MyClients') DROP TABLE [dbo].[MyClients]
CREATE TABLE [dbo].[MyClients](
	[MyClientsId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Phone] [nchar](20) NULL,
	[Email] [varchar](50) NOT NULL
 );

 ALTER TABLE [dbo].[MyClients] ADD CONSTRAINT PK_MyClients PRIMARY KEY (MyClientsId);

 