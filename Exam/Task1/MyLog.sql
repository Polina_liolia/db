use[MyExamDB]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'MyLog') DROP TABLE [dbo].[MyLog]
CREATE TABLE [dbo].[MyLog](
	[MyLogId] [int] IDENTITY(1,1) NOT NULL,
	[Action] [int] NOT NULL,
	[TBL_NAME] [varchar](20) NOT NULL,
	[ID_ROW] [int] NULL,
	[LOG_INFO] [varchar] (50) NULL,
	[LOG_TIME] [date] NOT NULL
 )

 ALTER TABLE [dbo].[MyLog] ADD CONSTRAINT PK_MyLog PRIMARY KEY (MyLogId);