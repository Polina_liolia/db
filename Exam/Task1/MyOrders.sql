use[MyExamDB]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'MyOrders') DROP TABLE [dbo].[MyOrders]
CREATE TABLE [dbo].[MyOrders](
	[MyOrdersId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](150) NULL,
	[OrdersDate] [date] NOT NULL,
	[TotalCost] [float] NOT NULL,
	[MyClientsId] [int] NOT NULL
 )

 ALTER TABLE [dbo].[MyOrders] ADD CONSTRAINT PK_MyOrders PRIMARY KEY (MyOrdersId);
 ALTER TABLE [dbo].[MyOrders] ADD CONSTRAINT FK_MyOrders_MyClientsId FOREIGN KEY (MyClientsId) REFERENCES [dbo].[MyClients](MyClientsId);