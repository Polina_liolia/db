use[MyExamDB]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'MyOrdersPositions') DROP TABLE [dbo].[MyOrdersPositions]
CREATE TABLE [dbo].[MyOrdersPositions](
	[MyOrdersPositionsId] [int] IDENTITY(1,1) NOT NULL,
	[MyOrdersId] [int] NOT NULL,
	[MyProductsId] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[Count] [int] NOT NULL
 )

 ALTER TABLE [dbo].[MyOrdersPositions] ADD CONSTRAINT PK_MyOrdersPositions PRIMARY KEY (MyOrdersPositionsId);
 ALTER TABLE [dbo].[MyOrdersPositions] ADD CONSTRAINT FK_MyOrdersPositions_MyOrdersId FOREIGN KEY (MyOrdersId) REFERENCES [dbo].[MyOrders](MyOrdersId);
 ALTER TABLE [dbo].[MyOrdersPositions] ADD CONSTRAINT FK_MyOrdersPositions_MyProductsId FOREIGN KEY (MyProductsId) REFERENCES [dbo].[MyProducts](MyProductsId);
