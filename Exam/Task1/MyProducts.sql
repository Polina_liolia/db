use[MyExamDB]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'MyProducts') DROP TABLE [dbo].[MyProducts]
CREATE TABLE [dbo].[MyProducts](
	[MyProductsId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Price] [float] NOT NULL
 )

 ALTER TABLE [dbo].[MyProducts] ADD CONSTRAINT PK_MyProducts PRIMARY KEY (MyProductsId);
