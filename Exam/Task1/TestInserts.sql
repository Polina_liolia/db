use[MyExamDB]

INSERT INTO [dbo].[MyClients] ([Name], [Phone], [Email]) VALUES ('Tom', '+380504125896544', 'tommy@gmail.com');
INSERT INTO [dbo].[MyClients] ([Name], [Email]) VALUES ('Ann', 'ann@gmail.com');
INSERT INTO [dbo].[MyClients] ([Name], [Phone], [Email]) VALUES ('Ken', '+380678521896128', 'kem@ukr.net');
INSERT INTO [dbo].[MyClients] ([Name], [Phone], [Email]) VALUES ('Jim', '+380987125296547', 'jim@mail.ru');
INSERT INTO [dbo].[MyClients] ([Name], [Email]) VALUES ('Liz', 'liz@ukr.net');

INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Chair', 480.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Sofa', 12720.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Kitchen table', 17500.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Office table', 2200.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Table-transformer for kids', 5490.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Chair for kids', 2400.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Bar chair', 3200.0);
INSERT INTO [dbo].[MyProducts] ([Name], [Price]) VALUES ('Arm-chair', 3740.0);

INSERT INTO [dbo].[MyOrders] ([Description], [MyClientsId], [OrdersDate], [TotalCost]) 
VALUES ('very important order', 1, '11.08.2017', 20200);
INSERT INTO [dbo].[MyOrders] ([MyClientsId], [OrdersDate], [TotalCost]) VALUES (2, '12.08.2017', 20380);
INSERT INTO [dbo].[MyOrders] ([MyClientsId], [OrdersDate], [TotalCost]) VALUES (5, '10.08.2017', 7890);
--INSERT INTO [dbo].[MyOrders] ([MyClientsId], [OrdersDate], [TotalCost]) VALUES (15, '10.08.2017', 7890);--ERROR - WRONG CLIENT ID


INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (1, 2, 12720, 1);
INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (1, 8, 3740, 2);
INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (2, 3, 17500, 1);
INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (2, 1, 480, 6);
INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (3, 5, 5490, 1);
INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (3, 6, 2400, 1);
--INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (4, 6, 2400, 1);--ERROR - WRONG ORDER ID
--INSERT INTO [dbo].[MyOrdersPositions] ([MyOrdersId], [MyProductsId], [Price], [Count]) VALUES (3, 55, 2400, 1);--ERROR - RONG PRODUCT ID

INSERT INTO [dbo].[MyLog] ([Action], [LOG_TIME], [TBL_NAME], [LOG_INFO]) VALUES (1, GETDATE(), 'MyClients', 'new table created');
INSERT INTO [dbo].[MyLog] ([Action], [LOG_TIME], [TBL_NAME]) VALUES (1, GETDATE(), 'MyData');
INSERT INTO [dbo].[MyLog] ([Action], [LOG_TIME], [TBL_NAME], [LOG_INFO]) VALUES (2, GETDATE(), 'MyData', 'table deleted');
INSERT INTO [dbo].[MyLog] ([Action], [LOG_TIME], [TBL_NAME], [LOG_INFO], [ID_ROW]) VALUES (3, GETDATE(), 'MyClients', 'new client added', 1);
INSERT INTO [dbo].[MyLog] ([Action], [LOG_TIME], [TBL_NAME], [LOG_INFO], [ID_ROW]) VALUES (4, GETDATE(), 'MyClients', 'client deleted', 6);