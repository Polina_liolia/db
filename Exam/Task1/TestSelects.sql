use[MyExamDB]

--updating tables to fill in whitespaces before select data:
update [dbo].[MyClients]
set Phone = 'unknown'
where Phone is NULL;

update [dbo].[MyOrders]
set Description = 'none'
where Description is NULL;

update [dbo].[MyLog]
set ID_ROW = -1
where ID_ROW is null

update [dbo].[MyLog]
set LOG_INFO = 'undefined'
where LOG_INFO is NULL;

--test selects

select
Clients.MyClientsId as ID,
Clients.Name,
Clients.Phone,
Clients.Email
from [dbo].[MyClients] as Clients

select 
Products.MyProductsId as ID,
Products.Name,
Products.Price as Price_UAH
from [dbo].[MyProducts] as Products

select
MyLog.MyLogId as ID,
MyLog.Action,
MyLog.LOG_TIME,
MyLog.TBL_NAME,
MyLog.ID_ROW,
MyLog.LOG_INFO
from [dbo].[MyLog] as MyLog

/*��� ������� ������ �������� ������, ������� ������� �������� � �������, ���� ������, ����������� �����
������ � � ����� ������, �� ��������� ��������� ������� �� �������.*/

select
Clients.MyClientsId as Client_ID,
Clients.Name as Client_Name,
Clients.Email,
Clients.Phone,
Orders.OrdersDate as Date,
Orders.TotalCost as Actual_Cost,
sum(OP.Price*OP.Count) as Calculated_Cost
from [dbo].[MyOrders] as Orders
left join [dbo].[MyOrdersPositions] as OP on OP.MyOrdersId = Orders.MyOrdersId
left join [dbo].[MyClients] as Clients on Clients.MyClientsId = Orders.MyClientsId
left join [dbo].[MyProducts] as Products on Products.MyProductsId = OP.MyProductsId
group by Clients.MyClientsId, Clients.Name, Clients.Email, Clients.Phone, Orders.OrdersDate, Orders.TotalCost



