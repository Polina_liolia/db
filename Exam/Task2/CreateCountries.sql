use [HRDataBase]
IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'Countries') DROP TABLE [dbo].[Countries]
CREATE TABLE [dbo].[Countries](
	[COUNTRY_ID] [INT] IDENTITY (1,1) NOT NULL,
	[COUNTRY_NAME] [nvarchar](50) NOT NULL,
	[REGION_ID] [INT] NOT NULL
 );

 ALTER TABLE [dbo].[Countries] ADD CONSTRAINT PK_Countries PRIMARY KEY (COUNTRY_ID);
 ALTER TABLE [dbo].[Countries] ADD CONSTRAINT FK_Countries_REGION_ID FOREIGN KEY (REGION_ID) REFERENCES [dbo].[regions](region_id);
