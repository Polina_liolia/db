use [HRDataBase]
IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'Departments') DROP TABLE [dbo].[Departments]
CREATE TABLE [dbo].[Departments](
	[DEPARTMENT_ID] [int] NOT NULL,
	[DEPARTMENT_NAME] [nvarchar](30) NOT NULL,
	[MANAGER_ID] [int] NULL,
	[LOCATION_ID] [int] NOT NULL
 );

 ALTER TABLE [dbo].[Departments] ADD CONSTRAINT PK_Departments PRIMARY KEY (DEPARTMENT_ID);
 ALTER TABLE [dbo].[Departments] ADD CONSTRAINT FK_Employees_LOCATION_ID FOREIGN KEY (LOCATION_ID) REFERENCES [dbo].[locations](location_id);