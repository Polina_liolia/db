use [HRDataBase]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'Employees') DROP TABLE [dbo].[Employees]
CREATE TABLE [dbo].[Employees](
	[EMPLOYEE_ID] [int] NOT NULL,
	[FIRST_NAME] [nvarchar](30) NOT NULL,
	[LAST_NAME] [nvarchar](30) NOT NULL,
	[EMAIL] [nvarchar](50) NULL,
	[PHONE_NUMBER] [nchar](20) NULL,
	[hire_date] [date] NULL,
	[JOB_ID] [varchar] (10) NOT NULL,
	[salary] [float] NOT NULL,
	[COMMISSION_PCT] [varchar] (10) NULL,
	[MANAGER_ID] [int] NULL,
	[DEPARTMENT_ID] [int] NULL
 );

 ALTER TABLE [dbo].[Employees] ADD CONSTRAINT PK_Employees PRIMARY KEY (EMPLOYEE_ID);
 ALTER TABLE [dbo].[Employees] ADD CONSTRAINT FK_Employees_DEPARTMENT_ID FOREIGN KEY (DEPARTMENT_ID) REFERENCES [dbo].[Departments](DEPARTMENT_ID);
 ALTER TABLE [dbo].[Employees] ADD CONSTRAINT FK_Employees_JOB_ID FOREIGN KEY (JOB_ID) REFERENCES [dbo].[jobs](JOB_ID);
 ALTER TABLE [dbo].[Employees] ADD CONSTRAINT FK_Employees_MANAGER_ID FOREIGN KEY (MANAGER_ID) REFERENCES [dbo].[Employees](EMPLOYEE_ID);
 
 


 