use [HRDataBase]
IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'job_history') DROP TABLE [dbo].[job_history]
CREATE TABLE [dbo].[job_history](
	[employee_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NULL,
	[job_id] [varchar] (10) NOT NULL,
	[department_id] [int] NOT NULL
 );

 ALTER TABLE [dbo].[job_history] ADD CONSTRAINT FK_job_history_employee_id FOREIGN KEY (employee_id) REFERENCES [dbo].[Employees](EMPLOYEE_ID);
 ALTER TABLE [dbo].[job_history] ADD CONSTRAINT FK_job_history_job_id FOREIGN KEY (job_id) REFERENCES [dbo].[jobs](JOB_ID);
 ALTER TABLE [dbo].[job_history] ADD CONSTRAINT FK_job_history_department_id FOREIGN KEY (department_id) REFERENCES [dbo].[Departments](DEPARTMENT_ID);
 
 