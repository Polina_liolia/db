use [HRDataBase]
IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'jobs') DROP TABLE [dbo].[jobs]
CREATE TABLE [dbo].[jobs](
	[JOB_ID] [varchar] (10) NOT NULL,
	[JOB_TITLE] [nvarchar](50) NOT NULL,
	[MIN_SALARY] [float] NOT NULL,
	[MAX_SALARY] [float] NOT NULL
 );

 ALTER TABLE [dbo].[jobs] ADD CONSTRAINT PK_jobs PRIMARY KEY (JOB_ID);