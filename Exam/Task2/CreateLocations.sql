use [HRDataBase]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'Locations') DROP TABLE [dbo].[Locations]
CREATE TABLE [dbo].[Locations](
	[Location_ID] [int] NOT NULL,
	[Street_address] [nvarchar](70) NOT NULL,
	[Postal_code] [nchar](5) NULL,
	[City] [nvarchar](30) NOT NULL,
	[State_province] [nvarchar](50) NULL,
	[Country_id] [int] NOT NULL
 );

 ALTER TABLE [dbo].[Locations] ADD CONSTRAINT PK_Locations PRIMARY KEY (Location_ID);
 ALTER TABLE [dbo].[Locations] ADD CONSTRAINT FK_Locations_Country_id FOREIGN KEY (Country_id) REFERENCES [dbo].[Countries](country_id);
 
 