use [HRDataBase]
IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'Regions') DROP TABLE [dbo].[Regions]
CREATE TABLE [dbo].[Regions](
	[REGION_ID] [INT] IDENTITY (1,1) NOT NULL,
	[REGION_NAME] [nvarchar](50) NOT NULL
 );

 ALTER TABLE [dbo].[Regions] ADD CONSTRAINT PK_Regions PRIMARY KEY (REGION_ID);