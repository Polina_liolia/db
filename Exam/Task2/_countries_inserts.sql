use [HRDataBase]

insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Ukraine', 1);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Poland', 1);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('GB', 2);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Germany', 2);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('France', 2);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Canada', 3);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Maxico', 3);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Brasil', 4);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Bogota', 4);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Egypt', 5);
insert into [dbo].[Countries] ([COUNTRY_NAME], [REGION_ID]) values ('Sudan', 5);
