use [HRDataBase]

insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (103, '2012/12/04', '2015/01/25', 'FI_ACCOUNT', 100);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (107, '2014/03/17', '2016/09/22', 'SA_MAN', 80);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (103, '2015/01/25', null, 'AC_ACCOUNT', 100);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (107, '2016/09/22', null, 'SA_REP', 80);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (129, '2016/08/01', null, 'MK_MAN', 20);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (137, '2008/05/08', null, 'HR_REP', 40);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (164, '2011/05/29', null, 'PR_REP', 70);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (127, '2013/12/02', '2014/12/09', 'ST_CLERK', 200);
insert into [dbo].[job_history] ([employee_id], [start_date], [end_date], [job_id], [department_id]) values (127, '2014/12/09', null, 'SH_CLERK', 200);