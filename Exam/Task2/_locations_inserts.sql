use [HRDataBase]

insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (1700, '25 Green Str.', 'Littletown', 2);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (1800, '1 Sumskaya Str.', 'Kharkiv', 1);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (2400, '78 Somestreet Str.', 'London', 3);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (1500, '55 Str.', 'Paris', 5);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (1400, '8 Empty Str.', 'Town1', 7);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (2700, '22 Close Av.', 'Bigcity', 6);
insert into [dbo].[Locations] ([Location_ID], [Street_address], [City], [Country_id]) values (2500, '31 MStreet Str.', 'Somecity', 8);