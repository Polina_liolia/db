USE [AdventureWorks2008]

--1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������
SELECT 
Employee.JobTitle,
PayHistory.PayFrequency
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 1
GROUP BY Employee.JobTitle, PayHistory.PayFrequency

SELECT 
Employee.JobTitle,
PayHistory.PayFrequency
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 2
GROUP BY Employee.JobTitle, PayHistory.PayFrequency

--2. ������ �������, � ������� �������� ���������� �� ����������, ������������ ��� � �����
SELECT 
Department.Name AS 'Department name',
PayHistory.PayFrequency
FROM [HumanResources].[Department] AS Department
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
ON Department.DepartmentID = DepartmentHistory.DepartmentID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON DepartmentHistory.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 1
GROUP BY Department.Name, PayHistory.PayFrequency
ORDER BY Department.Name;

--3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� �������
SELECT 
Person.LastName,
Person.MiddleName,
Person.FirstName,
Employee.JobTitle,
PayHistory.Rate,
PayHistory.PayFrequency
FROM [Person].[Person] AS Person
LEFT JOIN [HumanResources].[Employee] AS Employee
ON Person.BusinessEntityID = Employee.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Person.BusinessEntityID = PayHistory.BusinessEntityID
ORDER BY Person.LastName

--4. �������� ������ ����������� � ��������� ������, � ������� �� �������� ������ (��������: ������������ ����)
SELECT 
Person.BusinessEntityID,
Person.LastName,
Person.MiddleName,
Person.FirstName,
MAX(DepartmentHistory.StartDate) AS MAXStartDate,
Department.Name
FROM [Person].[Person] AS Person
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
ON Person.BusinessEntityID = DepartmentHistory.BusinessEntityID
LEFT JOIN [HumanResources].[Department] AS Department
ON Department.DepartmentID = DepartmentHistory.DepartmentID
GROUP BY Person.BusinessEntityID, Person.LastName,Person.MiddleName, Person.FirstName, Department.Name

--5. �������� ������ ������� � ������� ���������� ����� � ���
SELECT 
Department.Name,
AVG (PayHistory.Rate) AS 'AVERAGE SALARY'
FROM [HumanResources].[Department] AS Department
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
ON DepartmentHistory.DepartmentID = Department.DepartmentID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON PayHistory.BusinessEntityID = DepartmentHistory.BusinessEntityID
GROUP BY Department.Name

--6. �������� ������ ������� � ��������� ������ ������� �� ������ ����� ��� �����������
SELECT 
Department.Name,
SUM (PayHistory.Rate * 8 * 22) AS 'SALARY BUDGET'
FROM [HumanResources].[Department] AS Department
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
ON DepartmentHistory.DepartmentID = Department.DepartmentID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON PayHistory.BusinessEntityID = DepartmentHistory.BusinessEntityID
GROUP BY Department.Name