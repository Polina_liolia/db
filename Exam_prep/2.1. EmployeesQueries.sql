USE EmployeesInfoMs

--1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������
SELECT 
POSITIONS.POSITIONS_NAME,
EMPLOYEES.PayFrequency
FROM [dbo].[DEPARTMENTS] AS DEPARTMENTS
LEFT JOIN [dbo].[EMPLOYEES] AS EMPLOYEES 
ON EMPLOYEES.DEPARTMENTS_ID = DEPARTMENTS.DEPARTMENTS_ID
LEFT JOIN [dbo].[POSITIONS] AS POSITIONS
ON POSITIONS.POSITIONS_ID = EMPLOYEES.POSITIONS_ID
WHERE EMPLOYEES.PayFrequency = 1
GROUP BY POSITIONS.POSITIONS_NAME, EMPLOYEES.PayFrequency

SELECT 
POSITIONS.POSITIONS_NAME,
EMPLOYEES.PayFrequency
FROM [dbo].[DEPARTMENTS] AS DEPARTMENTS
LEFT JOIN [dbo].[EMPLOYEES] AS EMPLOYEES 
ON EMPLOYEES.DEPARTMENTS_ID = DEPARTMENTS.DEPARTMENTS_ID
LEFT JOIN [dbo].[POSITIONS] AS POSITIONS
ON POSITIONS.POSITIONS_ID = EMPLOYEES.POSITIONS_ID
WHERE EMPLOYEES.PayFrequency = 2
GROUP BY POSITIONS.POSITIONS_NAME, EMPLOYEES.PayFrequency

--2. ������ �������, � ������� �������� ���������� �� ����������, ������������ ��� � �����
SELECT 
DEPARTMENTS.DEPARTMENTS_NAME
FROM [dbo].[DEPARTMENTS] AS DEPARTMENTS
LEFT JOIN [dbo].[EMPLOYEES] AS EMPLOYEES 
ON EMPLOYEES.DEPARTMENTS_ID = DEPARTMENTS.DEPARTMENTS_ID
WHERE EMPLOYEES.PayFrequency = 1
GROUP BY DEPARTMENTS.DEPARTMENTS_NAME, EMPLOYEES.PayFrequency

--3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� �������
SELECT 
EMPLOYEES.EMPLOYEES_ID,
EMPLOYEES.EMPLOYEES_NAME,
POSITIONS.POSITIONS_NAME,
EMPLOYEES.SALARY,
EMPLOYEES.PayFrequency
FROM [dbo].[EMPLOYEES] AS EMPLOYEES
LEFT JOIN [dbo].[POSITIONS] AS POSITIONS
ON POSITIONS.POSITIONS_ID = EMPLOYEES.POSITIONS_ID

--4. �������� ������ ����������� � ��������� ������, � ������� �� �������� ������ (��������: ������������ ����)
SELECT 
EMPLOYEES.EMPLOYEES_ID,
EMPLOYEES.EMPLOYEES_NAME,
DEPARTMENTS.DEPARTMENTS_NAME
FROM [dbo].[EMPLOYEES] AS EMPLOYEES
LEFT JOIN [dbo].[DEPARTMENTS] AS DEPARTMENTS
ON DEPARTMENTS.DEPARTMENTS_ID = EMPLOYEES.DEPARTMENTS_ID

--5. �������� ������ ������� � ������� ���������� ����� � ���
SELECT 
DEPARTMENTS.DEPARTMENTS_NAME,
AVG(EMPLOYEES.SALARY * EMPLOYEES.PayFrequency) AS 'AVERAGE SALARY'
FROM [dbo].[DEPARTMENTS] AS DEPARTMENTS
LEFT JOIN [dbo].[EMPLOYEES] AS EMPLOYEES
ON DEPARTMENTS.DEPARTMENTS_ID = EMPLOYEES.DEPARTMENTS_ID
GROUP BY DEPARTMENTS.DEPARTMENTS_NAME

--6. �������� ������ ������� � ��������� ������ ������� �� ������ ����� ��� �����������
SELECT 
DEPARTMENTS.DEPARTMENTS_NAME,
SUM(EMPLOYEES.SALARY * 8 * 22) AS 'SALARY BUDGET'
FROM [dbo].[DEPARTMENTS] AS DEPARTMENTS
LEFT JOIN [dbo].[EMPLOYEES] AS EMPLOYEES
ON DEPARTMENTS.DEPARTMENTS_ID = EMPLOYEES.DEPARTMENTS_ID
GROUP BY DEPARTMENTS.DEPARTMENTS_NAME