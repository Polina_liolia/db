use [db28pr10]

IF EXISTS(SELECT 'True' FROM sys.tables WHERE name LIKE 'EmployeesToDepartments') DROP TABLE [dbo].[EmployeesToDepartments];

CREATE TABLE [dbo].[EmployeesToDepartments]
(
	id INT identity(1,1) NOT NULL,
	id_Employees INT NOT NULL,
	id_Departments INT NOT NULL
);

ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT PK_EmployeesToDepartments PRIMARY KEY (ID);--������ ��������� ����
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT FK_EmployeesToDepartments_id_Employees FOREIGN KEY (id_Employees) REFERENCES [DBO].[EMPLOYEES](ID);--������ ��������� ����
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT FK_EmployeesToDepartments_id_Departments FOREIGN KEY (id_Departments) REFERENCES [DBO].[DEPARTMENTS](ID);--������ ��������� ����

INSERT INTO [DBO].[EmployeesToDepartments] (id_Employees, id_Departments) VALUES (1, 4);
INSERT INTO [DBO].[EmployeesToDepartments] (id_Employees, id_Departments) VALUES (1, 2);
INSERT INTO [DBO].[EmployeesToDepartments] (id_Employees, id_Departments) VALUES (2, 3);

SELECT
DEP.[NAME],
MEN.[NAME],
MEN.[LNAME],
MEN.[YEAR]
FROM
[DBO].[DEPARTMENTS] DEP
LEFT JOIN [DBO].[EmployeesToDepartments] E2D
ON DEP.[ID] = E2D.[id_Departments]
LEFT JOIN [DBO].[EMPLOYEES] EM
ON EM.[ID] = E2D.[id_Employees]
LEFT JOIN [DBO].[MEN] MEN
ON EM.[ID_MEN] = MEN.[ID]
ORDER BY DEP.[NAME]


SELECT
MEN.[NAME],
MEN.[LNAME],
MEN.[YEAR],
DEP.[NAME]
FROM
[DBO].[EMPLOYEES] EM
LEFT JOIN [DBO].[EmployeesToDepartments] E2D
ON EM.[ID] = E2D.[id_Employees]
LEFT JOIN [DBO].[DEPARTMENTS] DEP
ON DEP.[ID] = E2D.[id_Departments]
LEFT JOIN [DBO].[MEN] MEN
ON EM.[ID_MEN] = MEN.[ID]
ORDER BY MEN.[NAME]


