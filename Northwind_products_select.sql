USE [NORTHWIND.MDF]
--������ ���� �������
SELECT 
*
FROM [DBO].[Products]

--������ ���� �������, ������������� �� �������� A-Z
SELECT 
*
FROM [DBO].[Products]
ORDER BY [DBO].[Products].[ProductName]

--������ ���� �������, ������������� �� �������� Z-A
SELECT 
*
FROM [DBO].[Products]
ORDER BY [DBO].[Products].[ProductName] desc 

--������ ���� �������, ������������� �� �������� A-Z � �� ����
SELECT 
*
FROM [DBO].[Products]
ORDER BY [DBO].[Products].[ProductName] asc,
[dbo].[Products].[UnitPrice] desc

--�������� ���� ����� ������� �����
SELECT top 1
*
FROM [DBO].[Products]
ORDER BY [dbo].[Products].[UnitPrice] desc

--������ 1996 ����
--SELECT
--[DBO].[Orders].[OrderID]
--FROM [DBO].[Orders]
--WHERE [DBO].[Orders].[OrderDate] >= '1996-01-01' AND [DBO].[Orders].[OrderDate] < '1997-01-01'

--������ ���� �������, ������� ����������� � 1996 ����
SELECT 
[DBO].[Orders].[OrderDate],
[DBO].[Products].[ProductName],
[dbo].[Order Details].[Quantity],
[dbo].[Order Details].[UnitPrice]
FROM [DBO].[Order Details]
LEFT JOIN [DBO].[Products] ON [DBO].[Products].[ProductID] = [DBO].[Order Details].[ProductID]
LEFT JOIN [DBO].[Orders] ON [DBO].[Orders].[OrderID] = [DBO].[Order Details].[OrderID]
WHERE [DBO].[Order Details].[OrderID] IN (
SELECT
[DBO].[Orders].[OrderID]
FROM [DBO].[Orders]
WHERE [DBO].[Orders].[OrderDate] >= '1996-01-01' AND [DBO].[Orders].[OrderDate] < '1997-01-01'
)


--������ ���� �������, ������� ����������� �� 2000 ����

SELECT 
[DBO].[Orders].[OrderDate],
[DBO].[Products].[ProductName],
[dbo].[Order Details].[Quantity],
[dbo].[Order Details].[UnitPrice]
FROM [DBO].[Order Details]
LEFT JOIN [DBO].[Products] ON [DBO].[Products].[ProductID] = [DBO].[Order Details].[ProductID]
LEFT JOIN [DBO].[Orders] ON [DBO].[Orders].[OrderID] = [DBO].[Order Details].[OrderID]
WHERE [DBO].[Order Details].[OrderID] IN (
SELECT
[DBO].[Orders].[OrderID]
FROM [DBO].[Orders]
WHERE [DBO].[Orders].[OrderDate] < '2000-01-01'
)