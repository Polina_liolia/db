USE [NORTHWIND.MDF];
--������� ���� �������������, � ������ ���� ������
SELECT 
[DBO].[Orders].[OrderDate],
[DBO].[Orders].[OrderID],
[DBO].[Orders].[CustomerID],
[DBO].[Customers].[ContactName],
[DBO].[Orders].[EmployeeID],
[DBO].[Employees].[FirstName] AS EMPLOYEES_FIRSTNAME
FROM [dbo].[Orders]
LEFT JOIN [dbo].[Customers]
ON [dbo].[Orders].[CustomerID] = [dbo].[Customers].[CustomerID]
LEFT JOIN [dbo].[Employees]
ON [DBO].[Orders].[EmployeeID] = [dbo].[Employees].EmployeeID

--���� �� � ������������ ������ � 1996 ����?
--1)������ �������� 1996 ����
--2)������ �������������, ������� ���� � ������ �������

--SELECT 
--[DBO].[Orders].[OrderDate],
--[DBO].[Orders].[OrderID],
--[DBO].[Orders].[CustomerID]
--FROM [DBO].[Orders]
--WHERE [DBO].[Orders].[OrderDate] >= '1996-01-01' AND [DBO].[Orders].[OrderDate] < '1997-01-01'

SELECT * 
FROM [DBO].[Customers]
WHERE [dbo].[Customers].[CustomerID] IN
(SELECT 
[DBO].[Orders].[CustomerID]
FROM [DBO].[Orders]
WHERE [DBO].[Orders].[OrderDate] >= '1996-01-01' AND [DBO].[Orders].[OrderDate] < '1997-01-01')