USE [AdventureWorks2008]

/*1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������*/
SELECT
EMPLOYEE.[JobTitle],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Employee] AS EMPLOYEE
LEFT JOIN [Person].[BusinessEntity] AS BusinessEntity
ON BusinessEntity.BusinessEntityID = EMPLOYEE.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeePayHistory.BusinessEntityID = EMPLOYEE.BusinessEntityID
WHERE EmployeePayHistory.[PayFrequency] = 1
UNION 
SELECT
EMPLOYEE.[JobTitle],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Employee] AS EMPLOYEE
LEFT JOIN [Person].[BusinessEntity] AS BusinessEntity
ON BusinessEntity.BusinessEntityID = EMPLOYEE.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeePayHistory.BusinessEntityID = EMPLOYEE.BusinessEntityID
WHERE EmployeePayHistory.[PayFrequency] = 2
ORDER BY EmployeePayHistory.[PayFrequency]

/*2. ������ �������, � ������� �������� ����������, �� ����������, ������������ ��� � �����*/
SELECT
DEPT.[DepartmentID],
DEPT.[Name] AS DEPARTMENT_NAME,
DEPT.[GroupName],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Department] AS DEPT
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS EmployeeDepartmentHistory
ON DEPT.[DepartmentID] = EmployeeDepartmentHistory.[DepartmentID]
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeeDepartmentHistory.BusinessEntityID = EmployeePayHistory.BusinessEntityID
WHERE EmployeePayHistory.[PayFrequency] = 1
ORDER BY DEPT.[DepartmentID]

/*3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� �������*/
SELECT 
PERSON.[LastName], 
PERSON.[FirstName],
PERSON.[MiddleName],
Employee.[JobTitle],
EmployeePayHistory.[Rate],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [Person].[Person] AS PERSON 
ON PERSON.[BusinessEntityID] = Employee.[BusinessEntityID]
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeePayHistory.[BusinessEntityID] = Employee.[BusinessEntityID]
ORDER BY PERSON.[LastName]

/*4. �������� ������ ����������� � ��������� ������, � ������� �� �������� ������ (��������: ������������ ����)*/
--��� ������� ������ ������ � ������������ �����?
SELECT 
Person.BusinessEntityID,
PERSON.[LastName], 
PERSON.[FirstName],
PERSON.[MiddleName],
Department.[Name] AS DEPARTMENT_NAME,
EmployeeDepartmentHistory.StartDate
--MAX(EmployeeDepartmentHistory.[StartDate])
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [Person].[Person] AS PERSON
ON PERSON.[BusinessEntityID] = Employee.[BusinessEntityID]
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS EmployeeDepartmentHistory
ON Employee.[BusinessEntityID] = EmployeeDepartmentHistory.[BusinessEntityID]
LEFT JOIN [HumanResources].[Department] AS Department
ON Department.[DepartmentID] = EmployeeDepartmentHistory.[DepartmentID]
--GROUP BY PERSON.[LastName] 
ORDER BY PERSON.[LastName]


/*5.�������� ������ ������� � ������� ���������� ����� � ���*/
SELECT
DEPT.[Name] AS DEPARTMENT_NAME,
AVG(EmployeePayHistory.[Rate]) AS AVERAGE_RATE
FROM [HumanResources].[Department] AS DEPT
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS EmployeeDepartmentHistory
ON DEPT.[DepartmentID] = EmployeeDepartmentHistory.[DepartmentID]
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeeDepartmentHistory.BusinessEntityID = EmployeePayHistory.BusinessEntityID
GROUP BY (DEPT.[Name])

/*6. �������� ������ ������� � ��������� ������ ������� �� ������ ����� ��� �����������*/
SELECT
DEPT.[Name] AS DEPARTMENT_NAME,
SUM(EmployeePayHistory.[Rate]) AS SUM_RATE --������ �� ���/����? ����� ������ ���� ���������?
FROM [HumanResources].[Department] AS DEPT
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS EmployeeDepartmentHistory
ON DEPT.[DepartmentID] = EmployeeDepartmentHistory.[DepartmentID]
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeeDepartmentHistory.BusinessEntityID = EmployeePayHistory.BusinessEntityID
GROUP BY (DEPT.[Name])