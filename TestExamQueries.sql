use master
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'TestExam') DROP DATABASE TestExam

CREATE DATABASE TestExam

CREATE TABLE [dbo].[MY_LOGS](
	[MY_LOGS_ID] [int] IDENTITY(1,1) NOT NULL,
	[ACTION] [nchar](3) NOT NULL,
	[TBL_NAME] [nvarchar](50) NOT NULL,
	[ID_ROW] [int] NOT NULL,
	[LOG_INFO] [nvarchar](50) NOT NULL,
	[LOG_TIME] [date] NOT NULL,
	[COLUMN_NAME] [nvarchar](50) NOT NULL,
	[OLD_VALUE] [nvarchar](50) NOT NULL,
	[NEW_VALUE] [nvarchar](50) NOT NULL
 )

 SELECT 
 *
 FROM [DBO].[MY_LOGS]