use [sqltest]

/*�������� ������ ������ �������, ������� ������� �����: �������������, ��� ������, ����*/
SELECT 
PC.[model],
PC.[price],
PRODUCT.[maker],
PRODUCT.[type],
'PC' AS PRODUCT_NAME
FROM [DBO].[PC] AS PC
LEFT JOIN [DBO].[Product] AS PRODUCT
ON PRODUCT.[model] = PC.[model]
UNION ALL --�����������, ����������� �������� �� ����������� �� �������
SELECT 
PRINTER.[model],
PRINTER.[price],
PRODUCT.[maker],
PRODUCT.[type],
'PRINTER' AS PRODUCT_NAME
FROM [DBO].[Printer] AS PRINTER
LEFT JOIN [DBO].[Product] AS PRODUCT
ON PRODUCT.[model] = PRINTER.[model]
UNION
SELECT
LAPTOP.[model],
LAPTOP.[price],
PRODUCT.[maker],
PRODUCT.[type],
'LAPTOP' AS PRODUCT_NAME
FROM [DBO].[Laptop] AS LAPTOP
LEFT JOIN [DBO].[Product] AS PRODUCT
ON PRODUCT.[model] = LAPTOP.[model]

/*�������� ������ ������ ���������*/
SELECT 
PRINTER.[model],
PRINTER.[price],
PRINTER.[color],
PRINTER.[type],
[DBO].[Product].[maker],
[DBO].[Product].[type]
FROM [DBO].[Printer] AS PRINTER
LEFT JOIN [DBO].[Product]
ON [DBO].[Product].[model] = PRINTER.[model]

/*�������� ������ ������ ������� ���������*/
SELECT 
PRINTERS.[model],
PRINTERS.[price],
PRINTERS.[color],
PRINTERS.[type],
[DBO].[Product].[maker],
[DBO].[Product].[type]
FROM [DBO].[Printer] AS PRINTERS
LEFT JOIN [DBO].[Product]
ON [DBO].[Product].[model] = PRINTERS.[model]
WHERE PRINTERS.color = 'y'

/*�������� ������� ��������� ��������*/
SELECT AVG(PRINTERS.[price]) AS AVERAGE_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS

/*�������� ������������ ��������� ��������*/
SELECT MAX(PRINTERS.[price]) AS MAX_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS

/*�������� ������� ��������� �������� ��������*/
SELECT AVG(PRINTERS.[price]) AS AVERAGE_COLOR_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS
WHERE PRINTERS.color = 'y'

/*�������� ������� ��������� �������� � ������ ��������� (type)*/
SELECT 
'Laser' AS PRINTER_TYPE,
AVG(PRINTERS.[price]) AS AVERAGE_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS
WHERE PRINTERS.[type] = 'Laser'
UNION ALL
SELECT
'Jet' AS PRINTER_TYPE, 
AVG(PRINTERS.[price]) AS AVERAGE_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS
WHERE PRINTERS.[type] = 'Jet'
UNION ALL
SELECT 
'Matrix' AS PRINTER_TYPE,
AVG(PRINTERS.[price]) AS AVERAGE_PRINTER_PRICE
FROM [DBO].[Printer] AS PRINTERS
WHERE PRINTERS.[type] = 'Matrix'

/*������� ������� ��������� ������ � ��������*/
/*������� ������������ ��������� ������ � ��������*/
SELECT 
'PRINTER' AS CATEGORY,
AVG(PRINTER.[price]) AS AVERAGE_PRICE,
MAX(PRINTER.[price]) AS MAX_PRICE
FROM [DBO].[Printer] AS PRINTER
UNION
SELECT 
'LAPTOP' AS CATEGORY,
AVG(LAPTOP.[price]) AS AVERAGE_PRICE,
MAX(LAPTOP.[price]) AS MAX_PRICE
FROM [DBO].[Laptop] AS LAPTOP
UNION
SELECT 
'PC' AS CATEGORY,
AVG(PC.[price]) AS AVERAGE_PRICE,
MAX(PC.[price]) AS MAX_PRICE
FROM [DBO].[PC] AS PC

/*�������� ������� �������� ������� ���������� ����� ��� �����������, ��� ��������� � �������*/
SELECT 
AVG(PC.[speed]) AS AVERAGE_PROCESSOR_SPEED_MHZ
FROM [DBO].[PC] AS PC
LEFT JOIN [DBO].[Product] AS PRODUCT
ON PRODUCT.[model] = PC.[model]
WHERE PC.[model] IN 
(
SELECT
PRODUCT.[MODEL]
FROM [DBO].[Product] AS PRODUCT
)

/*�������� ������ �����������, ��� ������� �������� ������� ��������� �������*/
SELECT
PC.[model],
PC.[price],
PC.[hd],
PC.[ram],
PC.[speed]
FROM [DBO].[PC] AS PC
WHERE PC.[speed] > (
SELECT 
AVG(PC.[speed])
FROM [DBO].[PC] AS PC)

/*������� ��������� � ������������ �������� ������. 13.1 �������� ������ ������ ��� � ������ ��� � �����������.*/
SELECT 
LAPTOP.[model],
Laptop.[screen] AS MAX_SCREEN_INCH,
Laptop.[screen]*2.54 AS MAX_SCREEN_SM
FROM [DBO].[Laptop] AS LAPTOP
WHERE Laptop.[screen] = (
SELECT
MAX(LAPTOP.[SCREEN])
FROM [DBO].[Laptop] AS LAPTOP
)

/*14. �������� ������ ���� ��������������, ������ ��� ������� �� ��� ����� ������� �����*/
SELECT
PRODUCT.[maker],
'LAPTOP' AS CATERORY,
LAPTOP.[model],
LAPTOP.[price]
FROM [DBO].[Product] AS PRODUCT
LEFT JOIN [DBO].[Laptop] AS LAPTOP
ON LAPTOP.[model] = PRODUCT.model
WHERE LAPTOP.[price] = (
SELECT 
MAX(LAPTOP.[price])
FROM [DBO].[Laptop] AS LAPTOP
)
UNION
SELECT
PRODUCT.[maker],
'PC' AS CATERORY,
PC.[model],
PC.[price]
FROM [DBO].[Product] AS PRODUCT
LEFT JOIN [DBO].[PC] AS PC
ON PC.[model] = PRODUCT.model
WHERE PC.[price] = (
SELECT 
MAX(PC.[price])
FROM [DBO].[PC] AS PC
)
UNION
SELECT
PRODUCT.[maker],
'PRINTER' AS CATERORY,
PRINTER.[model],
PRINTER.[price]
FROM [DBO].[Product] AS PRODUCT
LEFT JOIN [DBO].[PRINTER] AS PRINTER
ON PRINTER.[model] = PRODUCT.model
WHERE PRINTER.[price] = (
SELECT 
MAX(PRINTER.[price])
FROM [DBO].[PRINTER] AS PRINTER
)
ORDER BY (PRODUCT.[maker])